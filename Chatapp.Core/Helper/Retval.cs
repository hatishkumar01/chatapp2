﻿namespace Chatapp.Core.Utilities
{
    public class Retval<T>
    {
        public Retval()
        {
        }

        public Retval(int errcode, string message = null)
        {
            err_code = errcode;
            err_desc = message ?? GetDefaultMessageForStatusCode(errcode);
        }

        public T val { get; set; }

        public object ret_tag { get; set; }

        public int err_code { get; set; } = 0;

        public string err_desc { get; set; }

        public bool status { get; set; }

        public bool IsError()
        {
            return err_code != 0;
        }

        private string GetDefaultMessageForStatusCode(int statusCode)
        {
            switch (statusCode)
            {
                case 404:
                    return "Resource not found";
                case 400:
                    return err_desc;
                case 500:
                    return "An unhandled error occurred";
                default:
                    return null;
            }
        }
    }
}
