﻿

namespace Chatapp.Core.Helper
{
    public enum Role
    {
        Customer = 1,
        Agent = 2,
        Bot = 3
    }
}
