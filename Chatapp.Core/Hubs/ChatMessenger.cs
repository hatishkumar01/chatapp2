﻿using Chatapp.Core.Hubs;
using Chatapp.Core.Models;
using Chatapp.Data.Db;
using Chatapp.Data.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;

namespace Chatapp.Utilities.Hubs
{
    public class ChatMessenger : Hub, IHubClient
    {
        private readonly IRepositoryBase<User> _userService;
        private readonly static Dictionary<string, string> _ConnectionsMap = new Dictionary<string, string>();
        public readonly static List<UserChatModel> _Connections = new List<UserChatModel>();
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ChatMessenger(IRepositoryBase<User> userService, IHttpContextAccessor httpContextAccessor)
        {
            _userService = userService;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<List<UserChatModel>> GetOfflineUsers()
        {
            var users = await _userService.GetAll();
            var userEmails = _Connections.Select(x => x.UserEmail);

            return users.Where(x => !userEmails.Contains(x.Email)).Select(c => new UserChatModel()
            {
                UserEmail = c.Email,
                UserName = c.Name
            }).ToList();
        }

        public async Task<List<UserChatModel>> GetOnlineUsers() => _Connections;

        public async Task SendPrivateMessage(ChatModel message)
        {
            await Clients.User($"{message.MessageToEmail}").SendAsync("newMessage", message);
        }

        public async Task MessageToGroup(ChatModel message)
        {
            await Clients.Group($"{message.GroupName}").SendAsync("newMessage", message);
        }

        public IEnumerable<UserChatModel> GetUsers(string groupName)
        {
            return _Connections.Where(u => u.CurrentGroup == groupName).ToList();
        }

        public async Task Join(string groupName)
        {
            try
            {
                var user = _Connections.Where(u => u.UserEmail == IdentityName).FirstOrDefault();
                if (user != null && user.CurrentGroup != groupName)
                {
                    // Remove user from others list
                    if (!string.IsNullOrEmpty(user.CurrentGroup))
                        await Clients.OthersInGroup(user.CurrentGroup).SendAsync("removeUser", user);

                    // Join to new chat room
                    await Leave(user.CurrentGroup ?? "");
                    await Groups.AddToGroupAsync(Context.ConnectionId, groupName);
                    user.CurrentGroup = groupName;

                    // Tell others to update their list of users
                    await Clients.OthersInGroup(groupName).SendAsync("addUser", user);
                }
            }
            catch (Exception ex)
            {
                //await Clients.Caller.SendAsync("onError", "You failed to join the chat room!" + ex.Message);
            }
        }

        public async Task Leave(string groupName)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);
        }

        public async Task AddGroup(string groupName)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, groupName);
            //await Clients.Group(groupName).SendMessage(Context.User.Identity.Name + " joined.");
        }

        public IEnumerable<UserChatModel> GetUsersByGroup(string groupName)
        {
            return _Connections.Where(u => u.CurrentGroup == groupName).ToList();
        }

        public override Task OnConnectedAsync()
        {
            try
            {
                var user = _userService.GetList(u => u.Email == IdentityName).Result.FirstOrDefault();
                if (user != null)
                {
                    var userViewModel = new UserChatModel()
                    {
                        UserName = user.Name,
                        UserEmail = user.Email
                    };
                    userViewModel.Device = GetDevice();
                    userViewModel.CurrentGroup = "";

                    if (!_Connections.Any(u => u.UserEmail.ToLower().Trim() == IdentityName.ToLower().Trim()))
                    {
                        _Connections.Add(userViewModel);
                        _ConnectionsMap.Add(IdentityName, Context.ConnectionId);
                    }

                    Clients.Caller.SendAsync("getProfileInfo", userViewModel);
                }
            }
            catch (Exception ex)
            {
                Clients.Caller.SendAsync("onError", "OnConnected:" + ex.Message);
            }

            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            try
            {
                var user = _Connections.Where(u => u.UserEmail == IdentityName).First();
                _Connections.Remove(user);

                // Tell other users to remove you from their list
                Clients.OthersInGroup(user.CurrentGroup).SendAsync("removeUser", user);

                // Remove mapping
                _ConnectionsMap.Remove(user.UserEmail);
            }
            catch (Exception ex)
            {
                Clients.Caller.SendAsync("onError", "OnDisconnected: " + ex.Message);
            }

            return base.OnDisconnectedAsync(exception);
        }


        private string IdentityName
        {


            get
            {

                var email = _httpContextAccessor.HttpContext.User.Identity.Name;// .Identities.LastOrDefault().Name;
                return email;//Context.User.Identities.LastOrDefault().Name;
            } //Context.User.Identity.Name;
        }

        private string GetDevice()
        {
            //var device = Context.GetHttpContext().Request.Headers["Device"].ToString();
            //if (!string.IsNullOrEmpty(device) && (device.Equals("Desktop") || device.Equals("Mobile")))
            //    return device;

            return "Web";
        }
    }
}
