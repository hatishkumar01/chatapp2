﻿using Chatapp.Core.Models;
using Chatapp.Data.Db;

namespace Chatapp.Core.Hubs
{
    public interface IHubClient
    {
        //Task SendPrivateMessage(string connection, ChatModel message);
        //Task MessageToGroup(string connection, ChatModel msg);
        //Task InformGroup(string connection, UserChatModel msg);
        //Task SendMessage(string connection, UserChatModel msg);
        Task SendPrivateMessage(ChatModel message);
        Task<List<UserChatModel>> GetOfflineUsers();
        Task<List<UserChatModel>> GetOnlineUsers();

    }
}
