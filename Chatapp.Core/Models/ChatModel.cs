﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chatapp.Core.Models
{
    public class ChatModel
    {
        public int? Id { get; set; } = 0;
        public string? Content { get; set; }
        public string? MessageFromEmail { get; set; } = string.Empty;
        public string? MessageToEmail { get; set; } = string.Empty;// should be userEmail
        public DateTime? Timestamp { get; set; } = DateTime.UtcNow;
        public string? MediaPath { get; set; }
        public string? GroupName { get; set; } = string.Empty;
        public string? UserName { get; set; } = string.Empty;
        public bool IsPrivate { get; set; } = false;
        public IFormFile? mediaFileDetails { get; set; } = null;
    }


    public class ConversationModel
    {
        public string ConversationName { get; set; } = string.Empty;// User email or group name
        public List<CustomChatModel> PrivateMessages { get; set; } = new List<CustomChatModel>();// Private messages between two users
        public List<GroupMessageModel> GroupMessages { get; set; } = new List<GroupMessageModel>();// Group messages in this conversation
    }

    public class GroupMessageModel
    {
        public string GroupName { get; set; } // Group name
        public List<CustomChatModel> Chats { get; set; } = new List<CustomChatModel>(); // Messages within the group
    }

    public class CustomChatModel
    {
        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string Content { get; set; } = string.Empty;
        public string MessageFromEmail { get; set; } = string.Empty;
        public string MessageToEmail { get; set; } = string.Empty;
        public string GroupName { get; set; } = string.Empty;
        public bool IsPrivate { get; set; } = false;
        public string? MediaPath { get; set; } = string.Empty;
    }

}
