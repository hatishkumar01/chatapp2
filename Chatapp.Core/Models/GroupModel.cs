﻿using Chatapp.Data.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chatapp.Core.Models
{
    public class GroupModel
    {
        public int id { get; set; }

        public string name { get; set; }

        public int createdBy { get; set; }

        public DateTime createdOn { get; set; }

        public bool isDeleted { get; set; }

        public bool isPublic { get; set; }

        public virtual List<MessageModel> chatMessages { get; set; }

        public virtual User createdByNavigation { get; set; } = null!;
    }
}
