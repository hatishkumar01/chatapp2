﻿using Chatapp.Data.Db;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chatapp.Core.Models
{
    public class MessageModel
    {
        public int id { get; set; }
        public int? userId { get; set; }
        public string? message { get; set; }
        public string? media { get; set; }
        public DateTime dateTime { get; set; }
        //public int? groupId { get; set; }
        public string groupName { get; set; }
        //public int sentBy { get; set; }
        //public UserModel user { get; set; }
        //public GroupModel group { get; set; }
    }
}
