﻿
namespace Chatapp.Core.Models
{
    public class UserChatModel
    {
        public string UserEmail { get; set; }
        public string UserName { get; set; }
        public string CurrentGroup { get; set; }
        public string Device { get; set; }
    }
}
