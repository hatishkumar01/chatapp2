﻿namespace Chatapp.PageModels
{
    public class UserResponseModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public int role { get; set; }
        public string roleName { get; set; }

        public DateTimeOffset? lastLoginDate { get; set; }
    }
}
