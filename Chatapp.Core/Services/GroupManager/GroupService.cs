﻿using Chatapp.Core.Models;
using Chatapp.Core.Services.Messenger;
using Chatapp.Core.Utilities;
using Chatapp.Data.Db;
using Chatapp.Data.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;

namespace Chatapp.Core.Services.GroupManager
{
    public class GroupService : IGroupService
    {
        private readonly IRepositoryBase<ChatGroup> _chatGroup;
        private readonly IRepositoryBase<GroupMember> _groupmember;
        private readonly IRepositoryBase<User> _userService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMessengerService _messengerService;
        public GroupService(IRepositoryBase<ChatGroup> chatGroup,
            IRepositoryBase<GroupMember> GroupMember,
            IRepositoryBase<User> userService,
            IHttpContextAccessor httpContextAccessor,
            IMessengerService messengerService)
        {
            _chatGroup = chatGroup;
            _userService = userService;
            _httpContextAccessor = httpContextAccessor;
            _messengerService = messengerService;
            _groupmember = GroupMember;
        }

        public Task<ChatGroup> GetById(int groupId)
        {
            return _chatGroup.GetById(groupId);
        }

        public Task<bool> IsExist(string groupName)
        {
            return _chatGroup.Any(x => x.Name == groupName);
        }

        public Task<ChatGroup> GetByName(string groupName)
        {
            return _chatGroup.GetSingle(x => x.Name == groupName);
        }

        public async Task<List<GroupChatModel>> GetAll()
        {
            var result = await _chatGroup.GetAll();
            var roomsViewModel = result.Select(x => new GroupChatModel() { Name = x.Name, Id = x.Id, Admin = x.CreatedByNavigation?.Email ?? "--" }); //x.CreatedByNavigation?.Email

            return roomsViewModel.ToList();
        }

        public async Task<Retval<ChatGroup>> AddGroup(GroupChatModel groupChat)
        {
            string email = _httpContextAccessor.HttpContext.User.Identity.Name;
            var user = await _userService.GetSingle(x => x.Email == email);
            if (user == null) return new Retval<ChatGroup>(500, "User is not Exist");

            var group = new ChatGroup()
            {
                Name = groupChat.Name,
                CreatedBy = user.Id,
                CreatedOn = DateTime.Now,
                IsPublic = true,
            };

            await _chatGroup.Add(group);

            groupChat.Id = group.Id;
            groupChat.Admin = email;

            var listGroupMembers = new List<GroupMember>();

            if (groupChat.MemebersList != null && groupChat.MemebersList.Count() > 0 && group.Id > 0)
            {
                var validUsers = await _userService.GetMany(x => groupChat.MemebersList.Contains(x.Id));
                //if(validUsers.Count()!= groupChat.MemebersList.Count())
                //{
                //    return new Retval<ChatGroup>(500, "invalid members");
                //}

                if (validUsers != null)
                {
                    foreach (var member in validUsers)
                    {
                        var groupMember = new GroupMember()
                        {
                            Id = 0,
                            GroupId = group.Id,
                            MemberId = member.Id,
                        };
                        listGroupMembers.Add(groupMember);
                    }
                    await _groupmember.AddRange(listGroupMembers);
                }
            }

            await _messengerService.AddGroup(groupChat);
            return new Retval<ChatGroup>() { val = group };
        }
    }
}
