﻿using Chatapp.Core.Models;
using Chatapp.Core.Utilities;
using Chatapp.Data.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chatapp.Core.Services.GroupManager
{
    public interface IGroupService
    {
        Task<Retval<ChatGroup>> AddGroup(GroupChatModel groupChat);
        Task<ChatGroup> GetById(int groupId);
        Task<ChatGroup> GetByName(string groupName);
        Task<List<GroupChatModel>> GetAll();
        Task<bool> IsExist(string groupName);
    }
}
