﻿using Chatapp.Core.Models;
using Chatapp.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chatapp.Core.Services.Messenger
{
    public interface IMessengerService
    {
        Task<Retval<MessageModel>> Save(MessageModel messager);
        Task<Retval<MessageModel>> Update(MessageModel messager);
        Task<Retval<List<ChatModel>>> GetByUser(string userName);
        Task<Retval<List<MessageModel>>> GetAll();
        Task AddGroup(GroupChatModel roomView);
        Task<Retval<ChatModel>> SendMessage(ChatModel model);
        Task<Retval<List<ChatModel>>> GetMessagesByGroup(string groupName);
        Task<Retval<List<ChatModel>>> GetChatByUserEmail(string userEmail);
        Task<Retval<object>> GetAllGroupByMessagesbyUserName(string userEmail);
        Task<Retval<ChatModel>> ForwardChatToSenior(string UserEmail, string ClientEmail, string SeniorUserEmail);
    }
}
