﻿using Chatapp.Core.Helper;
using Chatapp.Core.Hubs;
using Chatapp.Core.Models;
using Chatapp.Core.Services.GroupManager;
using Chatapp.Core.Services.UserManager;
using Chatapp.Core.Utilities;
using Chatapp.Data.Db;
using Chatapp.Data.Repository;
using Chatapp.Utilities.Hubs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace Chatapp.Core.Services.Messenger
{
    public class MessengerService : IMessengerService
    {
        private readonly IHubContext<ChatMessenger> _hubContext;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IRepositoryBase<ChatMessage> _chatMessenger;
        private readonly IRepositoryBase<ChatGroup> _chatGroup;
        private readonly IRepositoryBase<User> _userService;
        protected readonly ChatdbContext _db;
        public MessengerService(IRepositoryBase<ChatMessage> chatMessenger,
            IRepositoryBase<ChatGroup> chatGroup,
            IRepositoryBase<User> userService,
            IHubContext<ChatMessenger> hubContext,
            IHttpContextAccessor httpContextAccessor,
            ChatdbContext db)
        {
            _chatMessenger = chatMessenger;
            _chatGroup = chatGroup;
            _hubContext = hubContext;
            _userService = userService;
            _httpContextAccessor = httpContextAccessor;
            this._db = db;
        }

        public async Task<Retval<MessageModel>> Save(MessageModel messager)
        {
            if (messager != null)
            {
                //ChatMessage entity = new ChatMessage()
                //{
                //    Message = messager.message,
                //    DateTime = messager.dateTime,
                //    UserId = (int)messager.userId
                //};

                //await _hubContext.Clients.All.SendMessageToAgent("newMessage", messager);
                //await _hubContext.Clients.Group(messager.groupName).SendAsync("newMessage", messager);
                //await _hubContext.Clients.User($"{messager.user}").ReceiveMessage(messager);

                //var isSaved = await _chatMessenger.Add(entity);
                //if (isSaved > 0) return new Retval<Messager>() { val = messager };
            }
            return new Retval<MessageModel>(500, Constants.ApiErrorMessage);
        }
        public async Task<Retval<ChatModel>> SendMessage(ChatModel model)
        {
            if (model.IsPrivate)
            {
                model = await SendPrivate(model);
            }
            else
            {
                model = await SendToGroup(model);
            }
            if (model != null)
            {
                model.Timestamp = DateTime.UtcNow;
                if (model != null) return new Retval<ChatModel>() { val = model };

                return new Retval<ChatModel>();
            }
            else
            {
                return new Retval<ChatModel>(500, "internal server error!");
            }
        }
        private async Task<ChatModel> SendPrivate(ChatModel model)
        {
            User loginUser = await GetLogedInUser();
            var sendTo = await _userService.GetSingle(u => u.Email == model.MessageToEmail);

            if (sendTo != null && loginUser != null)
            {
                var msg = new ChatMessage()
                {
                    Message = Regex.Replace(model.Content, @"<.*?>", string.Empty),
                    SentBy = loginUser.Id,
                    Media = model.MediaPath,
                    DateTime = DateTime.Now,
                    UserId = sendTo.Id
                };

                if (loginUser != null && !string.IsNullOrEmpty(loginUser.Name))
                {
                    model.MessageFromEmail = loginUser.Email;
                }

                await _hubContext.Clients.User(model.MessageToEmail).SendAsync("newMessage", model);
                await _chatMessenger.Add(msg);

                return model;
            }

            return null;
        }
        private async Task<ChatModel> SendToGroup(ChatModel model)
        {
            User loginUser = await GetLogedInUser();
            var group = await _chatGroup.GetSingle(r => r.Name == model.GroupName);

            if (group != null && loginUser != null)
            {
                var msg = new ChatMessage()
                {
                    //Message = Regex.Replace(model.Content ?? "-Media file -", @"<.*?>", string.Empty),
                    Message = model.Content ?? "",
                    SentBy = loginUser.Id,
                    GroupId = group.Id,
                    //Media = model?.MediaPath?.ToString(),
                    Media = model.MediaPath ?? "-- --",
                    DateTime = DateTime.Now,
                };

                if (loginUser != null && !string.IsNullOrEmpty(loginUser.Name))
                {
                    model.MessageFromEmail = loginUser.Email;
                }

                if (!string.IsNullOrEmpty(model?.MediaPath) && model.Content == null)
                {
                    model.Content = model.MediaPath;
                }

                await _hubContext.Clients.Group(model?.GroupName).SendAsync("newMessage", model);
                await _chatMessenger.Add(msg);
                if (msg.Id > 0) { model.Id = msg.Id; }
                return model;
            }

            return null;
        }
        private async Task<User> GetLogedInUser()
        {
            var email = _httpContextAccessor.HttpContext.User.Identity.Name;
            var loginUser = await _userService.GetSingle(u => u.Email == email);
            return loginUser;
        }
        public async Task AddGroup(GroupChatModel roomView)
        {
            var mssg = JsonConvert.SerializeObject(roomView);
            await _hubContext.Groups.AddToGroupAsync("addChatRoom", roomView.Name);
        }
        public async Task<Retval<MessageModel>> Update(MessageModel messager)
        {
            if (messager.id > 0)
            {
                ChatMessage entity = new ChatMessage()
                {
                    Id = messager.id,
                    Message = messager.message,
                    DateTime = messager.dateTime,
                    UserId = (int)messager.userId
                };

                var isSaved = await _chatMessenger.Update(entity);
                if (isSaved > 0) return new Retval<MessageModel>() { val = messager };
            }
            return new Retval<MessageModel>(500, Constants.ApiErrorMessage);
        }
        public async Task<Retval<List<ChatModel>>> GetByUser(string userEmail)
        {
            var user = await _userService.GetSingle(x => x.Email == userEmail);
            if (user != null)
            {
                var result = await _chatMessenger.GetMany(x => (x.SentBy == user.Id || x.UserId == user.Id));
                if (result != null)
                {
                    var messages = result.Where(x => x.UserId != null).Select(x => new ChatModel()
                    {
                        Id = x.Id,
                        Timestamp = x.DateTime,
                        Content = x.Message,
                        GroupName = (x.Group != null ? x.Group?.Name : "--"),
                        MessageFromEmail = (x.SentByNavigation != null ? x.SentByNavigation.Email : "--"),
                        MessageToEmail = (x.User != null ? x.User.Email : x.User.Email),
                        IsPrivate = (x.Group == null ? true : false)
                    }).ToList();

                    return new Retval<List<ChatModel>>() { val = messages };
                }
            }

            return new Retval<List<ChatModel>>();
        }
        public async Task<Retval<List<ChatModel>>> GetMessagesByGroup(string groupName)
        {
            var group = await _chatGroup.GetSingle(x => x.Name == groupName);
            if (group == null)
                return new Retval<List<ChatModel>>(500, "Gorup not found");

            var messages = await _chatMessenger.GetList(m => m.GroupId == group.Id,
                m => m.OrderByDescending(m => m.DateTime), "", 0, 20);

            if (messages.Count() > 0)
            {
                var chats = messages.Where(g => g.GroupId != null).Select(x => new ChatModel()
                {
                    Id = x.Id,
                    //Content = x.Message,
                    Content = x.Message ?? "-Media file -",
                    GroupName = x.Group?.Name,
                    Timestamp = x.DateTime,
                    MediaPath = x.Media ?? "-Media file -",
                    MessageFromEmail = (x.SentByNavigation != null ? x.SentByNavigation.Email : "--"),
                    MessageToEmail = (x.User != null ? x.User.Email : "--"),
                    UserName = (x.User != null ? x.User.Name : "--"),
                    //UserEmail = (x.User != null ? x.User.Email : "--"),
                }).ToList().OrderBy(x => x.Id).ToList();

                return new Retval<List<ChatModel>>() { val = chats };
            }
            return new Retval<List<ChatModel>>();
        }
        ////this below api for to send user chat to given email
        public async Task<Retval<List<ChatModel>>> GetChatByUserEmail(string userEmail)
        {
            var LogedInUser = await GetLogedInUser();
            if (LogedInUser != null)
            {
                var ChatWithUser = await _userService.GetSingle(x => x.Email == userEmail);
                if (ChatWithUser != null)
                {
                    var result = await _chatMessenger.GetMany(x => (x.SentBy == LogedInUser.Id && x.UserId == ChatWithUser.Id) ||
                                                                   (x.SentBy == ChatWithUser.Id && x.UserId == LogedInUser.Id));
                    if (result.Count() > 0)
                    {
                        var messages = result.Select(x => new ChatModel()
                        {
                            Id = x.Id,
                            Timestamp = x.DateTime,
                            Content = x.Message,
                            MessageToEmail = x.User?.Email,
                            MessageFromEmail = x.SentByNavigation?.Email,
                            GroupName = x.Group?.Name
                        }).ToList();

                        return new Retval<List<ChatModel>>() { val = messages };
                    }
                }
            }
            return new Retval<List<ChatModel>>();
        }
        public async Task<Retval<List<MessageModel>>> GetAll()
        {
            var result = await _chatMessenger.GetAll();
            if (result != null)
            {

                var data = result.Select(x => new MessageModel()
                {
                    id = x.Id,
                    message = x.Message,
                    userId = x.UserId,
                    dateTime = x.DateTime
                }).ToList();


                new Retval<List<MessageModel>>() { val = data };
            }

            return new Retval<List<MessageModel>>(500, Constants.ApiErrorMessage);
        }
        public async Task<Retval<object>> GetAllGroupByMessagesbyUserName(string userEmail)
        {
            //var user = await _userService.GetSingle(x => x.Email == userEmail);
            var userList = await _userService.GetAll();
            if (userList.Count() > 0)
            {
                var UserByEmail = userList.Where(x => x.Email == userEmail).FirstOrDefault();
                if (UserByEmail != null)
                {
                    var result = await _chatMessenger.GetList();
                    if (result.Count() > 0)
                    {
                        result = result.Where(x => (x.SentBy == UserByEmail.Id || x.UserId == UserByEmail.Id)).ToList();
                        var conversations = new List<ConversationModel>();

                        foreach (var message in result)
                        {
                            var messageUser = (message.UserId != null) ? userList.Where(s => s.Id == (int)message.UserId).FirstOrDefault() : null;
                            var chatModel = new CustomChatModel()
                            {
                                Id = message.Id,
                                Timestamp = message.DateTime,
                                Content = message.Message ?? "--",
                                MediaPath = message.Media ?? "--",
                                MessageFromEmail = (message.SentByNavigation != null ? message.SentByNavigation.Email : "--"),
                                MessageToEmail = (messageUser != null ? messageUser.Email : (message.Group != null) ? message.Group.Name : "--"),
                                IsPrivate = (message.GroupId != null ? false : true),
                            };

                            // Find or create a conversation based on the message sender or group
                            var conversation = conversations.FirstOrDefault(c =>
                                (c.ConversationName == chatModel.MessageFromEmail && chatModel.MessageToEmail == userEmail) ||
                                (c.ConversationName == chatModel.MessageToEmail && chatModel.MessageFromEmail == userEmail) ||
                                (c.ConversationName == message.Group?.Name));

                            if (conversation == null)
                            {
                                conversation = new ConversationModel
                                {
                                    ConversationName = chatModel.MessageFromEmail == userEmail ? chatModel.MessageToEmail : chatModel.MessageFromEmail
                                };

                                if (message.Group != null)
                                {
                                    conversation.GroupMessages = new List<GroupMessageModel>
                                    {
                                        new GroupMessageModel
                                        {
                                            GroupName = message.Group.Name,
                                            Chats = new List<CustomChatModel> { chatModel }
                                        }
                                    };
                                }
                                else
                                {
                                    conversation.PrivateMessages = new List<CustomChatModel> { chatModel };
                                }
                                conversations.Add(conversation);
                            }
                            else
                            {
                                if (message.Group != null)
                                {
                                    var groupMessage = conversation.GroupMessages.FirstOrDefault(g => g.GroupName == message.Group.Name);
                                    if (groupMessage == null)
                                    {
                                        groupMessage = new GroupMessageModel
                                        {
                                            GroupName = message.Group.Name
                                        };
                                        conversation.GroupMessages.Add(groupMessage);
                                    }
                                    groupMessage.Chats.Add(chatModel);
                                }
                                else
                                {
                                    conversation.PrivateMessages.Add(chatModel);
                                }
                            }
                        }
                        return new Retval<object>() { val = new { Conversations = conversations } };
                    }
                }
            }

            // Return a default value (null or empty) or handle the case where user is not found or no chat messages.
            return null; // You can adjust this to return an appropriate default value or handle the case differently.
        }

        //public async Task<Retval<ChatModel>> ForwardChatToSenior(string UserEmail, string ClientEmail, string SeniorUserEmail)
        //{
        //    var AllUsers = await _userService.GetAll();
        //    if (AllUsers.Count() <= 0)
        //    {
        //        return new Retval<ChatModel>(500, "something went worng!!.");
        //    }

        //    var ClientUser = AllUsers.Where(C => C.Email == ClientEmail).FirstOrDefault();
        //    var User = AllUsers.Where(C => C.Email == UserEmail).FirstOrDefault();
        //    var SeniorUser = AllUsers.Where(C => C.Email == SeniorUserEmail).FirstOrDefault();

        //    if (User == null || SeniorUser == null || ClientUser == null)
        //    {
        //        return new Retval<ChatModel>(400, "Invalid Emails!!");
        //    }

        //    //create  a new group like username1, username2, username3;
        //    //and make new entery eg. user added senior user something like 

        //    var newGroupName = User?.Name + ", " + ClientUser?.Name + ", " + SeniorUser?.Name;

        //    var isExists = await _db.ChatGroups.Where(c => c.Name.ToLower() == newGroupName.ToLower()).FirstOrDefaultAsync();
        //    if (isExists != null)
        //    {
        //        newGroupName += " -" + Guid.NewGuid().ToString().Substring(0, 4);
        //    }

        //    var group = new ChatGroup()
        //    {
        //        Name = newGroupName,
        //        CreatedBy = User.Id,
        //        CreatedOn = DateTime.Now,
        //        IsPublic = true,
        //    };

        //    await _chatGroup.Add(group);
        //    if (group == null || group.Id == 0)
        //    {
        //        return new Retval<ChatModel>(500, "something went worng!!");
        //    }

        //    var listGroupMembers = new List<GroupMember>();
        //    listGroupMembers.Add(new GroupMember()
        //    {
        //        Id = 0,
        //        GroupId = group.Id,
        //        MemberId = ClientUser.Id
        //    });

        //    listGroupMembers.Add(new GroupMember()
        //    {
        //        Id = 0,
        //        GroupId = group.Id,
        //        MemberId = SeniorUser.Id
        //    });

        //    listGroupMembers.Add(new GroupMember()
        //    {
        //        Id = 0,
        //        GroupId = group.Id,
        //        MemberId = User.Id
        //    });

        //    await _db.GroupMembers.AddRangeAsync(listGroupMembers);
        //    await _db.SaveChangesAsync();

        //    var allChatMessages = await _db.ChatMessages.Where(x => x.GroupId == null &&
        //                                                    ((x.SentBy == User.Id && x.UserId == ClientUser.Id) ||
        //                                                     (x.SentBy == ClientUser.Id && x.UserId == User.Id)))
        //                                          .ToListAsync();

        //    foreach (var chatMessage in allChatMessages)
        //    {
        //        chatMessage.GroupId = group.Id;
        //    }
        //    await _db.SaveChangesAsync();

        //    ChatModel MessageModel = new ChatModel()
        //    {
        //        Id = 0,
        //        Content = User.Name + " has created a new Group or added " + SeniorUser.Name + " into this group",
        //        IsPrivate = false,
        //        GroupName = group.Name,
        //        MessageFromEmail = User.Email,
        //    };

        //    MessageModel = await SendToGroup(MessageModel);
        //    if (MessageModel.Id >= 0)
        //    {
        //        return new Retval<ChatModel>(200, "successfully forwarded");
        //    }
        //    else { return new Retval<ChatModel>(500, "something went worng!!."); }
        //}

        public async Task<Retval<ChatModel>> ForwardChatToSenior(string UserEmail, string ClientEmail, string SeniorUserEmail)
        {
            // Retrieve all users
            var allUsers = await _userService.GetAll();

            // Check if there are no users
            if (!allUsers.Any())
            {
                return new Retval<ChatModel>(500, "Something went wrong.");
            }

            // Find the users based on their emails
            var clientUser = allUsers.FirstOrDefault(C => C.Email == ClientEmail);
            var user = allUsers.FirstOrDefault(C => C.Email == UserEmail);
            var seniorUser = allUsers.FirstOrDefault(C => C.Email == SeniorUserEmail);

            // Check if any of the users are not found
            if (user == null || seniorUser == null || clientUser == null)
            {
                return new Retval<ChatModel>(400, "Invalid Emails.");
            }

            // Create a new group name
            var newGroupName = $"{user.Name}, {clientUser.Name}, {seniorUser.Name}";

            // Check if the group name already exists
            var isExists = await _db.ChatGroups
                .Where(c => c.Name.ToLower() == newGroupName.ToLower())
                .FirstOrDefaultAsync();

            if (isExists != null)
            {
                newGroupName += " -" + Guid.NewGuid().ToString().Substring(0, 4);
            }

            var group = new ChatGroup()
            {
                Name = newGroupName,
                CreatedBy = user.Id,
                CreatedOn = DateTime.Now,
                IsPublic = true,
            };

            // Add the new group to the database
            await _chatGroup.Add(group);

            if (group == null || group.Id == 0)
            {
                return new Retval<ChatModel>(500, "Something went wrong.");
            }

            var listGroupMembers = new List<GroupMember>()
            {
                new GroupMember() { GroupId = group.Id, MemberId = clientUser.Id },
                new GroupMember() { GroupId = group.Id, MemberId = seniorUser.Id },
                new GroupMember() { GroupId = group.Id, MemberId = user.Id }
            };

            GroupChatModel groupChatModel = new GroupChatModel()
            { Id = 0, Name = group.Name, MemebersList = listGroupMembers.Select(x => x.MemberId).Distinct().ToList() };
            await AddGroup(groupChatModel);

            // Add group members to the database
            await _db.GroupMembers.AddRangeAsync(listGroupMembers);
            await _db.SaveChangesAsync();

            // Update existing chat messages to use the new group
            var allChatMessages = await _db.ChatMessages
                .Where(x => x.GroupId == null &&
                            ((x.SentBy == user.Id && x.UserId == clientUser.Id) ||
                             (x.SentBy == clientUser.Id && x.UserId == user.Id)))
                .ToListAsync();

            foreach (var chatMessage in allChatMessages)
            {
                chatMessage.GroupId = group.Id;
            }

            await _db.SaveChangesAsync();

            // Create a notification message
            var notificationMessage = new ChatModel()
            {
                Content = $"{user.Name} has created a new group or added {seniorUser.Name} into this group",
                IsPrivate = false,
                GroupName = group.Name,
                MessageFromEmail = user.Email,
            };

            // Send the notification message to the group
            notificationMessage = await SendToGroup(notificationMessage);

            if (notificationMessage.Id >= 0)
            {
                return new Retval<ChatModel>(200, "Successfully forwarded.");
            }
            else
            {
                return new Retval<ChatModel>(500, "Something went wrong.");
            }
        }
    }
}
