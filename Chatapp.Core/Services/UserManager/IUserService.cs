﻿using Chatapp.Core.Models;
using Chatapp.Core.Utilities;
using Chatapp.PageModels;

namespace Chatapp.Core.Services.UserManager
{
    public interface IUserService
    {
        Task<UserModel> LogIn(string username, string password);
        Task<Retval<int>> Save(UserModel userModel);
        Task<Retval<UserModel>> GetById(int userId);
        Task<Retval<List<UserChatModel>>> GetUsers();
        Task<UserModel> SetUser(string userEmail);
        Task<UserResponseModel> GetLastLoginUser();
    }
}
