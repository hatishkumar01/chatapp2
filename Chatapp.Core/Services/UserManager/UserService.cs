﻿using Chatapp.Core.Helper;
using Chatapp.Core.Models;
using Chatapp.Core.Utilities;
using Chatapp.Data.Db;
using Chatapp.Data.Repository;
using Chatapp.PageModels;
using Microsoft.AspNetCore.Http;

namespace Chatapp.Core.Services.UserManager
{
    public class UserService : IUserService
    {
        private readonly IRepositoryBase<User> _userService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserService(IRepositoryBase<User> repositoryBase, IHttpContextAccessor httpContextAccessor)
        {
            _userService = repositoryBase;
            _httpContextAccessor = httpContextAccessor;
        }
        public async Task<UserModel> LogIn(string username, string password)
        {
            var result = await _userService.GetList(x => x.Name == username && x.Password == password);

            if (result.Count() > 0)
            {
                var user = result.FirstOrDefault();
                return new UserModel()
                {
                    id = user.Id,
                    name = user.Name,
                    email = user.Email
                };
            }

            return null;
        }

        public async Task<Retval<UserModel>> GetById(int userId)
        {
            var result = await _userService.GetById(userId);

            if (result != null)
            {
                var data = new UserModel()
                {
                    id = result.Id,
                    name = result.Name,
                    email = result.Email
                };

                return new Retval<UserModel>() { val = data };
            }

            return new Retval<UserModel>(500, Constants.ApiErrorMessage);
        }

        public async Task<Retval<List<UserChatModel>>> GetUsers()
        {
            string currentUser = _httpContextAccessor.HttpContext.User.Identity.Name;
            //var result = await _userService.GetMany(x => x.Email != currentUser);
            var result = await _userService.GetMany(x => x.Email != null);

            if (result != null)
            {
                var data = result.Select(x => new UserChatModel()
                {
                    UserEmail = x.Email,
                    UserName = x.Name,
                }).ToList();

                return new Retval<List<UserChatModel>>() { val = data };
            }

            return new Retval<List<UserChatModel>>(500, Constants.ApiErrorMessage);
        }

        public async Task<Retval<int>> Save(UserModel userModel)
        {
            if (userModel != null)
            {
                User entity = new User()
                {
                    Name = userModel.name,
                    Email = userModel.email,
                    Password = userModel.password,
                    Role = userModel.role
                };

                int id = await _userService.Add(entity);
                return new Retval<int>() { val = id };
            }

            return new Retval<int>(500, Constants.ApiErrorMessage);
        }

        public async Task<UserModel> SetUser(string userEmail)
        {
            var result = await _userService.GetList(x => x.Email.ToLower() == userEmail.ToLower());

            if (result.Count() > 0)
            {
                var user = result.FirstOrDefault();
                if (user != null)
                {
                    user.LastLoginDate = DateTimeOffset.UtcNow;
                    await _userService.Update(user);

                    return new UserModel()
                    {
                        id = user.Id,
                        name = user.Name,
                        email = user.Email
                    };
                }
            }
            return null;
        }
        public async Task<UserResponseModel> GetLastLoginUser()
        {
            var result = await _userService.GetcustomListWithAllIncludes();
            if (result.Count() > 0)
            {
                var user = result.Where(x=> x.LastLoginDate!= null).OrderByDescending(x => x.LastLoginDate).FirstOrDefault();
                if (user != null)
                {
                    return new UserResponseModel()
                    {
                        id = user.Id,
                        name = user.Name,
                        email = user.Email,
                        role = user.Role,
                        lastLoginDate = user.LastLoginDate,
                        roleName = user.RoleNavigation?.Role1,
                    };
                }
            }
            return null;
        }
    }
}
