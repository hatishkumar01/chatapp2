﻿using System;
using System.Collections.Generic;

namespace Chatapp.Data.Db;

public partial class ChatGroup
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public int CreatedBy { get; set; }

    public DateTime CreatedOn { get; set; }

    public bool IsDeleted { get; set; }

    public bool IsPublic { get; set; }

    public virtual ICollection<ChatMessage> ChatMessages { get; set; } = new List<ChatMessage>();

    public virtual User CreatedByNavigation { get; set; } = null!;
}
