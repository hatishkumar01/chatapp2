﻿using System;
using System.Collections.Generic;

namespace Chatapp.Data.Db;

public partial class ChatMessage
{
    public int Id { get; set; }

    public int? UserId { get; set; }

    public string? Message { get; set; }

    public string? Media { get; set; }

    public DateTime DateTime { get; set; }

    public int? GroupId { get; set; }

    public int SentBy { get; set; }

    public virtual ChatGroup? Group { get; set; }

    public virtual User SentByNavigation { get; set; } = null!;

    public virtual User? User { get; set; }
}
