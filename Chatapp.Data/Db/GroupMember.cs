﻿using System;
using System.Collections.Generic;

namespace Chatapp.Data.Db;

public partial class GroupMember
{
    public int Id { get; set; }

    public int GroupId { get; set; }

    public int MemberId { get; set; }
}
