﻿using System;
using System.Collections.Generic;

namespace Chatapp.Data.Db;

public partial class User
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string Email { get; set; } = null!;

    public string? Password { get; set; }

    public int Role { get; set; }

    public DateTimeOffset? LastLoginDate { get; set; }

    public virtual ICollection<ChatGroup> ChatGroups { get; set; } = new List<ChatGroup>();

    public virtual ICollection<ChatMessage> ChatMessageSentByNavigations { get; set; } = new List<ChatMessage>();

    public virtual ICollection<ChatMessage> ChatMessageUsers { get; set; } = new List<ChatMessage>();

    public virtual Role RoleNavigation { get; set; } = null!;
}
