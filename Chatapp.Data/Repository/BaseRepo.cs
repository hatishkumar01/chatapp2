﻿using Chatapp.Data.Db;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Chatapp.Data.Repository
{
    public interface IRepositoryBase<TEntity> : IDisposable where TEntity : class
    {
        Task<int> Add(TEntity obj);
        Task<int> AddRange(List<TEntity> objs);
        Task<TEntity> GetById(int id);
        Task<IEnumerable<TEntity>> GetAll();
        Task<int> Update(TEntity obj);
        Task<int> Remove(TEntity obj);
        Task<IEnumerable<TEntity>> GetList(
             Expression<Func<TEntity, bool>>? filter = null,
             Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
             string includeProperties = "",
             int skip = 0,
             int take = 0);

        Task<IEnumerable<TEntity>> GetcustomListWithAllIncludes(
             Expression<Func<TEntity, bool>>? filter = null,
             Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
             string includeProperties = "",
             int skip = 0,
             int take = 0);

        Task<TEntity> GetSingle(Expression<Func<TEntity, bool>>? filter = null);

        Task<int> Count(Expression<Func<TEntity, bool>> where);

        Task<bool> Any(Expression<Func<TEntity, bool>> where);

        Task<IEnumerable<TEntity>> GetMany(Expression<Func<TEntity, bool>> where);


    }

    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    {
        protected readonly ChatdbContext db;// = new ChatdbContext();
        private readonly DbSet<TEntity> _dbSet;

        public RepositoryBase(ChatdbContext context)
        {
            db = context;
            _dbSet = db.Set<TEntity>();
        }

        public virtual async Task<int> Add(TEntity obj)
        {
            db.Add(obj);
            return await db.SaveChangesAsync();
        }

        public virtual async Task<int> AddRange(List<TEntity> obj)
        {
            db.AddRange(obj);
            return await db.SaveChangesAsync();
        }

        //public virtual async Task<IEnumerable<TEntity>> GetAll() =>
        //    await db.Set<TEntity>().ToListAsync();

        public virtual async Task<TEntity> GetById(int id) =>
            await db.Set<TEntity>().FindAsync(id);

        public virtual async Task<IEnumerable<TEntity>> GetAll()
        {
            IQueryable<TEntity> query = db.Set<TEntity>();
            var entityType = db.Model.FindEntityType(typeof(TEntity));
            if (entityType != null)
            {
                foreach (var navigation in entityType.GetNavigations())
                {
                    query = query.Include(navigation.Name);
                }
            }
            return await query.ToListAsync();
        }

        public virtual async Task<int> Remove(TEntity obj)
        {
            db.Set<TEntity>().Remove(obj);
            return await db.SaveChangesAsync();
        }

        public virtual async Task<int> Update(TEntity obj)
        {
            db.Entry(obj).State = EntityState.Modified;
            return await db.SaveChangesAsync();
        }

        //public virtual async Task<IEnumerable<TEntity>> GetList(
        //     Expression<Func<TEntity, bool>>? filter = null,
        //     Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
        //     string includeProperties = "",
        //     int skip = 0,
        //     int take = 0)
        //{
        //    IQueryable<TEntity> query = _dbSet;
        //    if (filter != null)
        //        query = query.Where(filter);

        //    query = includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

        //    if (orderBy != null)
        //        query = orderBy(query);

        //    if (skip != 0)
        //        _ = query.Skip(skip);

        //    if (take != 0)
        //        _ = query.Take(take);

        //    return await query.ToListAsync();
        //}
        public virtual async Task<IEnumerable<TEntity>> GetList(
    Expression<Func<TEntity, bool>>? filter = null,
    Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
    string includeProperties = "",
    int skip = 0,
    int take = 0)
        {
            IQueryable<TEntity> query = _dbSet;

            // Apply the filter expression
            if (filter != null)
            {
                query = query.Where(filter);
            }

            // Apply dynamic includes
            foreach (var includeProperty in includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            // Apply ordering
            if (orderBy != null)
            {
                query = orderBy(query);
            }

            // Apply skip and take
            if (skip != 0)
            {
                query = query.Skip(skip);
            }

            if (take != 0)
            {
                query = query.Take(take);
            }

            // Use left outer join for included properties
            foreach (var includeProperty in includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty).DefaultIfEmpty();
            }

            return await query.ToListAsync();
        }
         public virtual async Task<IEnumerable<TEntity>> GetcustomListWithAllIncludes(
    Expression<Func<TEntity, bool>>? filter = null,
    Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
    string includeProperties = "",
    int skip = 0,
    int take = 0)
        {
            IQueryable<TEntity> query = _dbSet;

            // Apply the filter expression
            if (filter != null)
            {
                query = query.Where(filter);
            }

            // Apply dynamic includes
            foreach (var includeProperty in includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            // Apply ordering
            if (orderBy != null)
            {
                query = orderBy(query);
            }

            // Apply skip and take
            if (skip != 0)
            {
                query = query.Skip(skip);
            }

            if (take != 0)
            {
                query = query.Take(take);
            }

            var entityType = db.Model.FindEntityType(typeof(TEntity));
            if (entityType != null)
            {
                foreach (var navigation in entityType.GetNavigations())
                {
                    query = query.Include(navigation.Name);
                }
            }

            return await query.ToListAsync();
        }
        
        public virtual async Task<TEntity> GetSingle(Expression<Func<TEntity, bool>>? filter = null)
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            var entityType = db.Model.FindEntityType(typeof(TEntity));

            if (entityType != null)
            {
                foreach (var navigation in entityType.GetNavigations())
                {
                    query = query.Include(navigation.Name);
                }
            }

            return await query.FirstOrDefaultAsync();
        }

        public virtual async Task<int> Count(Expression<Func<TEntity, bool>> where) => await _dbSet.CountAsync(where);

        public virtual async Task<bool> Any(Expression<Func<TEntity, bool>> where) => await _dbSet.AnyAsync(where);

        //public virtual async Task<IEnumerable<TEntity>> GetMany(Expression<Func<TEntity, bool>> where) => await _dbSet.Where(where).ToListAsync();
        //public virtual async Task<IEnumerable<TEntity>> GetMany(Expression<Func<TEntity, bool>> where)
        //{
        //    IQueryable<TEntity> query = _dbSet;

        //    // Apply the filter expression
        //    if (where != null)
        //    {
        //        query = query.Where(where);
        //    }

        //    // Include all related entities dynamically
        //    var entityType = db.Model.FindEntityType(typeof(TEntity));

        //    if (entityType != null)
        //    {
        //        foreach (var navigation in entityType.GetNavigations())
        //        {
        //            query = query.Include(navigation.Name);
        //        }
        //    }

        //    return await query.ToListAsync();
        //}
        public virtual async Task<IEnumerable<TEntity>> GetMany(Expression<Func<TEntity, bool>> where)
        {
            IQueryable<TEntity> query = _dbSet;

            // Apply the filter expression
            if (where != null)
            {
                query = query.Where(where);
            }

            var entityType = db.Model.FindEntityType(typeof(TEntity));
            
            if (entityType != null)
            {
                foreach (var navigation in entityType.GetNavigations())
                {
                    query = query.Include(navigation.Name);
                }
            }

            return await query.ToListAsync();
        }

        public void Dispose() =>
            db.Dispose();
    }
}
