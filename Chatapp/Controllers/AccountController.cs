﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Chatapp.Core.Hubs;
using Chatapp.Core.Services.UserManager;
using Microsoft.AspNetCore.SignalR;

namespace Chatapp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IHubClient _hubContext;
        public AccountController(IUserService userService, IHubClient hubContext)
        {
            _userService = userService;
            _hubContext = hubContext;
        }

        [HttpPost("setuser")]
        public async Task<ActionResult> setuser(string userEmail)
        {
            if (string.IsNullOrEmpty(userEmail))
                return BadRequest("email is required");

            var result = await _userService.SetUser(userEmail);
            if (result != null)
            {
                var claims = new List<Claim>() {
                         new Claim(ClaimTypes.Name, userEmail),
                         };

                var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var principal = new ClaimsPrincipal(identity);

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

                return Ok("successful login.");
            }
            else { return BadRequest("somthing went worng"); }
        }
    }
}
