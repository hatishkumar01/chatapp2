﻿using Chatapp.Core.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Chatapp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        protected ObjectResult GenError<T>(Retval<T> obj)
        {
            return StatusCode(obj.err_code, obj.err_desc);
        }
    }
}
