﻿using Chatapp.Core.Models;
using Chatapp.Core.Services.Messenger;
using Chatapp.Core.Services.UserManager;
using Chatapp.Data.Db;
using Chatapp.Utilities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.DotNet.Scaffolding.Shared.Messaging;
using Newtonsoft.Json;
using System;
using System.Security.Claims;
using System.Text.RegularExpressions;

namespace Chatapp.Controllers
{
    [Route("api/[controller]")]
    public class ChatController : BaseController
    {
        private readonly IMessengerService _messengerService;
        private readonly IUserService _userService;
        private readonly IWebHostEnvironment _environment;
        private readonly ChatdbContext _context;
        private readonly EmailHelper _emailHelper;

        public ChatController(IMessengerService messengerService, IWebHostEnvironment environment, IUserService userService, ChatdbContext context, EmailHelper emailHelper)
        {
            _messengerService = messengerService;
            _userService = userService;
            _environment = environment;
            _context = context;
            _emailHelper = emailHelper;
        }

        //[HttpGet("messagesbyuser/{userEmail}")]
        //public async Task<ActionResult> GetMessageByUser(string userEmail)
        //{
        //    var result = await _messengerService.GetByUser(userEmail);
        //    if (result.IsError()) return GenError(result);

        //    if (result.val == null)
        //        return NotFound();

        //    return Ok(result.val);
        //}


        //[HttpGet("messagesAllMessagesbyuser/{userEmail}")]
        //public async Task<ActionResult> GetAllMessagesByUser(string userEmail)
        //{
        //    var result = await _messengerService.GetByUser(userEmail);
        //    if (result.IsError()) return GenError(result);

        //    if (result.val == null)
        //        return NotFound();

        //    return Ok(result.val);
        //}


        [HttpGet("GetAllUserMessagesAsGroupByMessages/{userEmail}")]
        public async Task<ActionResult> GetAllGroupByMessages(string userEmail)
        {
            var result = await _messengerService.GetAllGroupByMessagesbyUserName(userEmail);
            if (result.IsError()) return GenError(result);

            if (result.val == null)
                return NotFound();

            return Ok(result.val);
        }

        [HttpGet("messagesbygroup/{groupName}")]
        public async Task<IActionResult> GetMessagesByGroup(string groupName)
        {
            var result = await _messengerService.GetMessagesByGroup(groupName);
            if (!result.IsError()) return Ok(result);
            return GenError(result);
        }

        [HttpPost("sendmessage")]
        public async Task<ActionResult<ChatModel>> SendMessage(ChatModel chatModel)
        {
            var result = await _messengerService.SendMessage(chatModel);
            if (!result.IsError()) return Ok(result);

            return GenError(result);
            //return CreatedAtAction(nameof(GetMessage), new { id = result.val.Id }, chatModel);
        }

        [HttpGet("SendGroupChatOnMail/{emailTo}/{GroupName}")]
        public async Task<IActionResult> SendChatOnEmailByGroupName(string emailTo, string GroupName)
        {
            if (emailTo == null)
            { return BadRequest("Invalid request! Please enter valid email address"); }

            if (GroupName == null)
            { return BadRequest("Invalid request! Please enter valid group name"); }

            var result = await _messengerService.GetMessagesByGroup(GroupName);
            if (result.IsError()) return GenError(result);

            if (result.val.Count() > 0)
            {
                string toEmail = emailTo;
                string subject = "Your all Chat from ChatApp";
                string body = "<h1>This is your complete chat.</h1> Please find attachments" + "\n";
                string JSONData = JsonConvert.SerializeObject(result.val);

                var lres = _emailHelper.SendEmailWithAttachments(toEmail, subject, body, JSONData);
                if (lres)
                    return Ok("Success, chat is sended on the mentioned email:" + emailTo);
            }
            return BadRequest("something went wrong");
        }

        [HttpGet("SendUserChatOnMail/{emailTo}/{userEmail}")]
        public async Task<IActionResult> SendChatOnEmailByUserEmail(string emailTo, string userEmail)
        {
            if (string.IsNullOrEmpty(emailTo))
            { return BadRequest("Invalid request! Please enter valid ToEmail address"); }

            if (string.IsNullOrEmpty(userEmail))
            { return BadRequest("Invalid request! Please enter valid user email"); }

            var result = await _messengerService.GetChatByUserEmail(userEmail);
            if (result.IsError()) return GenError(result);

            if (result.val.Count() > 0)
            {
                string toEmail = emailTo;
                string subject = "Your all Chat from ChatApp";
                string body = "<h1>This is your complete chat.</h1>";
                string JSONData = JsonConvert.SerializeObject(result.val);
                var lres = _emailHelper.SendEmailWithAttachments(toEmail, subject, body, JSONData);
                if (lres)
                    return Ok("Success, chat is sended on the mentioned email:" + emailTo);
            }
            else
                return NotFound("chat not found");

            return BadRequest("something went wrong");
        }

        [HttpPost("SendMessageWithMediaUpload")]
        public async Task<IActionResult> SendMessageWithMediaSupport([FromForm] ChatModel chatModel)
        {
            if (chatModel.MessageFromEmail == null && chatModel.mediaFileDetails == null && chatModel?.MessageToEmail == null)
                return BadRequest("Validation failed!");

            if (chatModel.mediaFileDetails == null && string.IsNullOrEmpty(chatModel?.Content))
                return BadRequest("Validation failed!");

            if (chatModel.IsPrivate == true && string.IsNullOrEmpty(chatModel.MessageToEmail))
            { return BadRequest("Validation failed!"); }

            if (chatModel.Timestamp == null)
                chatModel.Timestamp = DateTime.Now;

            var fileName = DateTime.Now.ToString("yyyymmddMMss") + "_" + Path.GetFileName(chatModel?.mediaFileDetails?.FileName);
            if (chatModel?.mediaFileDetails != null)
            {
                var folderPath = Path.Combine(_environment.WebRootPath, "uploads");
                var filePath = Path.Combine(folderPath, fileName);

                if (!Directory.Exists(folderPath))
                    Directory.CreateDirectory(folderPath);

                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    await chatModel.mediaFileDetails.CopyToAsync(fileStream);
                }

                //chatModel.MediaPath = filePath;
                string htmlImage = string.Format(
                    "<a href=\"/uploads/{0}\" target=\"_blank\">" +
                    "<img src=\"/uploads/{0}\" class=\"post-image\">" +
                    "</a>", fileName);

                // Combine the image HTML and the message
                chatModel.Content = string.Format(
                    "{0}<p>{1}</p>",
                    htmlImage,
                    chatModel.Content ?? "");

                chatModel.MediaPath = Regex.Replace(htmlImage ?? "-Media file -", @"(?i)<(?!img|a|/a|/img).*?>", string.Empty);
            }

            var result = await _messengerService.SendMessage(chatModel);
            if (!result.IsError()) return Ok(result);

            return GenError(result);
        }



        [HttpGet("ForwardThePrivateChat")]
        public async Task<IActionResult> ForwardThePrivateChat(string UserEmail, string ClientEmail, string SeniorUserEmail)
        {
            if (string.IsNullOrEmpty(UserEmail))
            { return BadRequest("Invalid request! Please enter valid User Email address"); }

            if (string.IsNullOrEmpty(ClientEmail))
            { return BadRequest("Invalid request! Please enter valid Client Email address"); }

            if (string.IsNullOrEmpty(SeniorUserEmail))
            { return BadRequest("Invalid request! Please enter valid user email address"); }

            var result = await _messengerService.ForwardChatToSenior(UserEmail, ClientEmail, SeniorUserEmail);
            if (result.IsError()) return GenError(result);

            return BadRequest("something went wrong");
        }
    }
}
