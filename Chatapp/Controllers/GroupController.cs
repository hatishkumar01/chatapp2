﻿using Chatapp.Core.Models;
using Chatapp.Core.Services.GroupManager;
using Chatapp.Core.Services.UserManager;
using Chatapp.Data.Db;
using Chatapp.PageModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Chatapp.Controllers
{
    [Route("api/[controller]")]
    public class GroupController : BaseController
    {
        public readonly IGroupService _groupService;

        public GroupController(IGroupService groupService)
        {
            _groupService = groupService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get(int id)
        {
            var groups = await _groupService.GetById(id);
            if(groups == null)
            {
                return Ok("no record found!!");
            }
            return Ok(groups);
        }

        [HttpGet("Groups")]
        public async Task<ActionResult> GetGroups()
        {
            var groups = await _groupService.GetAll();

            return Ok(groups);
        }

        //[HttpPost("CreateGroup")]
        //public async Task<ActionResult> CreateGroup(GroupChatModel group)
        //{
        //    if (_groupService.IsExist(group.Name).Result)
        //        return BadRequest("Invalid room name or room already exists");

        //    var result = await _groupService.AddGroup(group);
        //    if (result.IsError()) return GenError(result);

        //    return CreatedAtAction(nameof(Get), new { id = result.val.Id }, group);
        //}

        [HttpPost("CreateRoom")]
        public async Task<IActionResult> CreateRoomAsync([FromBody] CreateRoomReqModel model)
        {
            if (string.IsNullOrEmpty(model.Name))
            { return BadRequest("Invalid request"); }

            if (_groupService.IsExist(model.Name).Result)
                return BadRequest("Invalid room name or room already exists");

            GroupChatModel groupChatModel = new GroupChatModel()
            { Id = 0, Name = model.Name };

            var result = await _groupService.AddGroup(groupChatModel);
            if (result.IsError()) return GenError(result);

            return CreatedAtAction(nameof(Get), new { id = result.val.Id }, groupChatModel);
        }

        [HttpPost("CreateRoomWithMembers")]
        public async Task<IActionResult> CreateRoomAsync([FromBody] CustomCreateRoomReqModel model)
        {
            if (string.IsNullOrEmpty(model.Name))
            { return BadRequest("Invalid request"); }

            if (model.MemebersList.Count() <= 0)
            { return BadRequest("Invalid request! Please select a member"); }

            if (_groupService.IsExist(model.Name).Result)
                return BadRequest("Invalid room name or room already exists");

            GroupChatModel groupChatModel = new GroupChatModel()
            { Id = 0, Name = model.Name, MemebersList = model.MemebersList.Distinct().ToList() };

            var result = await _groupService.AddGroup(groupChatModel);
            if (result.IsError()) return GenError(result);

            return CreatedAtAction(nameof(Get), new { id = result.val.Id }, groupChatModel);
        }
    }
}
