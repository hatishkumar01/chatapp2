﻿using Chatapp.Core.Hubs;
using Chatapp.Core.Services.UserManager;
using Chatapp.Utilities.Hubs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using NuGet.Protocol;

namespace Chatapp.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    public class UserController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IHubClient _hubContext;

        public UserController(IUserService userService, IHubClient hubContext)
        {
            _userService = userService;
            _hubContext = hubContext;
        }

        [HttpGet("GetUsers")]
        public async Task<ActionResult> GetUsers()
        {
            var result = await _userService.GetUsers();
            if (result.IsError()) return GenError(result);

            return Ok(result.val);
        }

        [HttpGet("GetOnlineUsers")]
        public async Task<ActionResult> GetOnlineUsers()
        {
            var user = _hubContext.GetOnlineUsers();
            return Ok(user);
        }

        [HttpGet("GetOfflineUsers")]
        public async Task<ActionResult> GetOfflineUsers()
        {
            var result = await _hubContext.GetOfflineUsers();
            return Ok(result);
        }

        //last visiting URL// 
        [HttpGet("GetSessionTabInfo")]
        public IActionResult GetSessionTabInfo()
        {
            // Assuming user authentication has been implemented.
            var username = User?.Identity?.Name ?? "--";
            var requestInfo = new
            {
                SourceIpAddress = HttpContext.Connection.RemoteIpAddress?.ToString(),
                RequestUrl = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}{HttpContext.Request.Path}{HttpContext.Request.QueryString}",
                HttpMethod = HttpContext.Request.Method,
                UserAgent = HttpContext.Request.Headers["User-Agent"],
                Cookies = HttpContext.Request.Cookies.ToDictionary(kv => kv.Key, kv => kv.Value),
                QueryParameters = HttpContext.Request.Query,
                Headers = HttpContext.Request.Headers,
                RequestedFrom = HttpContext.Request.Headers["Referer"],
                Username = username // Assign the authenticated user's username
            };

            return Ok(requestInfo);
        }

        [HttpGet("lastloginUserDetails")]
        public async Task<IActionResult> GetlastloginUserDetails()
        {
            var result = await _userService.GetLastLoginUser();
            if (result != null)
                return Ok(result);
            else
                return NotFound("no user login yet..");
        }
    }
}
