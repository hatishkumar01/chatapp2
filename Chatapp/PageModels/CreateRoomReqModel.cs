﻿namespace Chatapp.PageModels
{
    public class CreateRoomReqModel
    {
        public string Name { get; set; }
    }

    public class CustomCreateRoomReqModel
    {
        public string Name { get; set; }
        public List<int> MemebersList { get; set; }
    }
}
