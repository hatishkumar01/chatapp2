﻿using System.ComponentModel.DataAnnotations;

namespace Chatapp.PageModels
{
    public class RegisterModel
    {
        [Required]
        internal string Name { get; set; } = null!;
        [Required]
        internal string Email { get; set; } = null!;
        [Required(ErrorMessage = "Password is required")]
        [StringLength(255, ErrorMessage = "Must be between 5 and 255 characters", MinimumLength = 5)]
        [DataType(DataType.Password)]
        internal string Password { get; set; }
        [Required(ErrorMessage = "Confirm Password is required")]
        [StringLength(255, ErrorMessage = "Must be between 5 and 255 characters", MinimumLength = 5)]
        [DataType(DataType.Password)]
        [Compare("Password")]
        internal string ConfirmPassword { get; set; }
        [Required]
        private int role { get; set; }
    }
}
