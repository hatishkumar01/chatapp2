using Chatapp.Data.Db;
using Chatapp.Utilities;
using Chatapp.Utilities.Hubs;
using Microsoft.AspNetCore.Authentication.Cookies;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddDbContext<ChatdbContext>();
//builder.Services.AddAuthentication();
builder.Services.AddHttpContextAccessor();
builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(op => op.LoginPath = "/account/login");
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle


builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
builder.Services.RegisterServices();

// Add services to the container.
builder.Services.AddRazorPages();

//builder.Services.AddTransient<Hub<IHubClient>, ChatMessenger>();
builder.Services.AddTransient<ChatMessenger>();
builder.Services.AddSignalR();

builder.Services.AddMvcCore().AddViews();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
	app.UseSwagger();
	app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.MapControllerRoute(
	name: "Client",
	pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

//app.UseMvc();
app.UseStaticFiles();

app.UseMiddleware<ExceptionHandlingMiddleware>();

app.MapRazorPages();
 
app.MapHub<ChatMessenger>("/Messenger");


app.Run();
