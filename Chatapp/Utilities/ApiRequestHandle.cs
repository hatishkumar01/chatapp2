﻿using System.Net.Http.Headers;
using System.Net;
using System.Text.Json;
using System.Text;

namespace Chatapp.Utilities
{
    public class ApiRequestHandle
    {
        private readonly string baseUrl;
        private readonly HttpClient client;
        private readonly JsonSerializerOptions jsonSerializerOptions;

        public ApiRequestHandle(string baseUrl)
        {
            this.baseUrl = baseUrl;
            this.client = new HttpClient();
            this.jsonSerializerOptions = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true,
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            };
        }

        public async Task<T> GetAsync<T>(string url, WebHeaderCollection headers = null, bool allowRedirects = true, CancellationToken cancellationToken = default)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, baseUrl + url);
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            if (headers != null)
            {
                foreach (string key in headers.Keys)
                {
                    request.Headers.Add(key, headers[key]);
                }
            }

            var handler = new HttpClientHandler();
            handler.AllowAutoRedirect = allowRedirects;

            using (var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false))
            {
                response.EnsureSuccessStatusCode();

                try
                {
                    var result = await response.Content.ReadFromJsonAsync<T>(jsonSerializerOptions, cancellationToken).ConfigureAwait(false);
                    return result;
                }
                catch (Exception ex)
                {
                    throw new HttpRequestException($"Error deserializing response: {ex.Message}", ex);
                }
            }
        }

        public async Task<T> PostAsync<T>(string url, object data, WebHeaderCollection headers = null, CancellationToken cancellationToken = default)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, baseUrl + url);
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            if (headers != null)
            {
                foreach (string key in headers.Keys)
                {
                    request.Headers.Add(key, headers[key]);
                }
            }

            try
            {
                var content = new StringContent(JsonSerializer.Serialize(data, jsonSerializerOptions), Encoding.UTF8, "application/json");
                request.Content = content;

                var handler = new HttpClientHandler();

                using (var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false))
                {
                    response.EnsureSuccessStatusCode();

                    try
                    {
                        var result = await response.Content.ReadFromJsonAsync<T>(jsonSerializerOptions, cancellationToken).ConfigureAwait(false);
                        return result;
                    }
                    catch (Exception ex)
                    {
                        throw new HttpRequestException($"Error deserializing response: {ex.Message}", ex);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new HttpRequestException($"Error serializing request body: {ex.Message}", ex);
            }
        }
    }
}
