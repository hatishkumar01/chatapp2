﻿using System.Net.Mail;
using System.Net;
using System.Text;

namespace Chatapp.Utilities
{
    public class EmailHelper
    {
        private readonly IConfiguration _configuration;

        public EmailHelper(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public bool SendEmail(string toEmail, string subject, string body)
        {
            string smtpServer = _configuration["EmailSettings:SmtpServer"];
            int smtpPort = int.Parse(_configuration["EmailSettings:SmtpPort"]);
            string username = _configuration["EmailSettings:Username"];
            string password = _configuration["EmailSettings:Password"];

            using (SmtpClient smtpClient = new SmtpClient(smtpServer, smtpPort))
            {
                smtpClient.EnableSsl = true;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(username, password);

                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(username);
                mailMessage.To.Add(toEmail);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;

                try
                {
                    smtpClient.Send(mailMessage);
                    Console.WriteLine("Email sent successfully.");
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error sending email: " + ex.Message);
                    return false;
                }
            }
        }

        public bool SendEmailWithAttachments(string toEmail, string subject, string body, string JSONFileData)
        {
            string smtpServer = _configuration["EmailSettings:SmtpServer"];
            int smtpPort = int.Parse(_configuration["EmailSettings:SmtpPort"]);
            string username = _configuration["EmailSettings:Username"];
            string password = _configuration["EmailSettings:Password"];

            using (SmtpClient smtpClient = new SmtpClient(smtpServer, smtpPort))
            {
                smtpClient.EnableSsl = true;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(username, password);

                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(username);
                mailMessage.To.Add(toEmail);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;

                // Attach the JSON file data
                byte[] jsonDataBytes = Encoding.UTF8.GetBytes(JSONFileData);
                MemoryStream ms = new MemoryStream(jsonDataBytes);
                mailMessage.Attachments.Add(new Attachment(ms, "data.json", "application/json"));

                try
                {
                    smtpClient.Send(mailMessage);
                    Console.WriteLine("Email sent successfully.");
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error sending email: " + ex.Message);
                    return false;
                }
            }
        }
    }
}
