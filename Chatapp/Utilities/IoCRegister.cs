﻿using Chatapp.Core.Hubs;
using Chatapp.Core.Services.GroupManager;
using Chatapp.Core.Services.Messenger;
using Chatapp.Core.Services.UserManager;
using Chatapp.Data.Repository;
using Chatapp.Utilities.Hubs;
using Microsoft.AspNetCore.SignalR;

namespace Chatapp.Utilities
{
    public static class IoCRegister
    {
        public static IServiceCollection RegisterServices(this IServiceCollection services)
        {
            services.AddScoped(typeof(IRepositoryBase<>), typeof(RepositoryBase<>));
            services.AddScoped<IMessengerService, MessengerService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IGroupService, GroupService>();
            services.AddScoped<IHubClient, ChatMessenger>();
            services.AddScoped<EmailHelper, EmailHelper>();

            return services;
        }
    }
}
