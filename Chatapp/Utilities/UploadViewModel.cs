﻿
using System.ComponentModel.DataAnnotations;

namespace Chatapp.Utilities
{
    public class UploadViewModel
    {

        [Required]
        public string? Content { get; set; }
        public string MessageFromEmail { get; set; } = "";
        public string? MessageToEmail { get; set; } = ""; 
        public DateTime Timestamp { get; set; } = DateTime.UtcNow;
        public int? GroupId { get; set; }
        public string? MediaPath { get; set; }
        public bool IsPrivate { get; set; }
        public IFormFile? FileDetails { get; set; }
    }
}
