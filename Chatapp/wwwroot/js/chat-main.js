﻿$(document).ready(function () {
    var connection = new signalR.HubConnectionBuilder().withUrl("/Messenger").build();

    connection.start().then(function () {
        console.log('SignalR Started...')
        viewModel.groupList();
        viewModel.userList();
    }).catch(function (err) {
        return console.error(err);
    });

    connection.on("newMessage", function (messageView) {
        console.log("messageView ==>", messageView);
        var isMine = messageView.fromUserName === viewModel.myProfile().userName();
        var message = new ChatMessage(messageView.id, messageView.content, messageView.timestamp, messageView.fromUserName, messageView.fromFullName, isMine, messageView.avatar);
        viewModel.chatMessages.push(message);
        $(".messages-container").animate({ scrollTop: $(".messages-container")[0].scrollHeight }, 1000);
    });

    connection.on("getProfileInfo", function (user) {
        console.log(user);
        viewModel.myProfile(new ProfileInfo(user.userEmail, user.fullName, user.avatar));
        viewModel.isLoading(false);
    });

    connection.on("addUser", function (user) {
        console.log(user);
        viewModel.userAdded(new ChatUser(user.userName, user.fullName, user.avatar, user.currentGroup, user.device));
    });

    connection.on("removeUser", function (user) {
        console.log(user);
        viewModel.userRemoved(user.userName);
    });

    connection.on("addChatRoom", function (Group) {
        console.log("Group ==>", Group);
        viewModel.GroupAdded(new ChatGroup(Group.id, Group.name, Group.admin));
    });

    connection.on("updateChatGroup", function (Group) {
        viewModel.GroupUpdated(new ChatGroup(Group.id, Group.name, Group.admin));
    });

    connection.on("removeChatGroup", function (id) {
        viewModel.GroupDeleted(id);
    });

    connection.on("removeChatMessage", function (id) {
        viewModel.messageDeleted(id);
    });

    connection.on("onError", function (message) {
        viewModel.serverInfoMessage(message);
        $("#errorAlert").removeClass("d-none").show().delay(5000).fadeOut(500);
    });

    connection.on("onGroupDeleted", function () {
        if (viewModel.chatGroups().length == 0) {
            viewModel.joinedGroup(null);
        }
        else {
            // Join to the first Group from the list
            viewModel.joinGroup(viewModel.chatGroups()[0]);
        }
    });

    function AppViewModel() {
        var self = this;

        self.message = ko.observable("");
        self.chatGroups = ko.observableArray([]);
        self.chatUsers = ko.observableArray([]);
        self.chatMessages = ko.observableArray([]);
        self.joinedGroup = ko.observable();
        self.serverInfoMessage = ko.observable("");
        self.myProfile = ko.observable();
        self.isLoading = ko.observable(false);

        //self.showAvatar = ko.computed(function () {
        //    return self.isLoading() == false && self.myProfile().avatar() != null;
        //});

        self.showGroupActions = ko.computed(function () {
            console.log(self.joinedGroup()?.admin() + " ---- " + self.myProfile()?.userName())
            return self.joinedGroup()?.name() == self.myProfile()?.userName();
        });

        self.onEnter = function (d, e) {
            if (e.keyCode === 13) {
                self.sendNewMessage();
            }
            return true;
        }
        self.filter = ko.observable("");
        self.filteredChatUsers = ko.computed(function () {
            if (!self.filter()) {
                return self.chatUsers();
            } else {
                return ko.utils.arrayFilter(self.chatUsers(), function (user) {
                    var fullName = user.fullName().toLowerCase();
                    return fullName.includes(self.filter().toLowerCase());
                });
            }
        });

        self.sendNewMessage = function () {
            var text = self.message();

            if (text.startsWith("/")) {
                var receiver = text.substring(text.indexOf("(") + 1, text.indexOf(")"));
                var message = text.substring(text.indexOf(")") + 1, text.length);
                self.sendPrivate(receiver, message);
            }
            else {
                self.sendToGroup(self.joinedGroup(), self.message());
            }

            self.message("");
        }

        self.sendToGroup = function (Group, message) {
            if (Group.name()?.length > 0 && message.length > 0) {
                fetch('/api/Chat/sendmessage', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    //body: JSON.stringify({ Group: Group.name(), content: message })
                    body: JSON.stringify(
                        //{ groupName: Group.name(), content: message, userEmail: "praveen@gmail.com", userName: "praveen" }
                        { groupName: Group.name(), content: message }
                    )
                });
            }
        }

        self.sendPrivate = function (receiver, message) {
            if (receiver.length > 0 && message.length > 0) {
                fetch('/api/Chat/sendmessage', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(
                        { content: message.trim(), MessageToEmail: receiver.trim(), IsPrivate: true }
                    )
                });
            }
        }

        self.joinGroup = function (Group) {
            connection.invoke("Join", Group.name()).then(function () {
                self.joinedGroup(Group);
                self.userList();
                self.messageHistory();
            });
        }

        self.groupList = function () {
            fetch('/api/Group/Groups')
                .then(response => response.json())
                .then(data => {
                    self.chatGroups.removeAll();
                    for (var i = 0; i < data.length; i++) {
                        self.chatGroups.push(new ChatGroup(data[i].id, data[i].name, data[i].admin));
                    }

                    if (self.chatGroups().length > 0)
                        self.joinGroup(self.chatGroups()[0]);
                });
        }

        self.userList = function () {
            connection.invoke("GetUsers", self.joinedGroup()?.name()).then(function (result) {
                self.chatUsers.removeAll();
                for (var i = 0; i < result.length; i++) {
                    self.chatUsers.push(new ChatUser(result[i].userEmail,
                        result[i].userName,
                        result[i].currentGroup,
                        result[i].device))
                }
            });
        }

        self.createGroup = function () {
            var RoomName = $("#roomName").val();
            fetch('/api/Group/CreateRoom', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ name: RoomName })
            });
        }

        self.editGroup = function () {
            var GroupId = self.joinedGroup().id();
            var GroupName = $("#newGroupName").val();
            fetch('/api/Group/' + GroupId, {
                method: 'PUT',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ id: GroupId, name: GroupName })
            });
        }

        self.deleteGroup = function () {
            fetch('/api/Group/' + self.joinedGroup().id(), {
                method: 'DELETE',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ id: self.joinedGroup().id() })
            });
        }

        self.deleteMessage = function () {
            var messageId = $("#itemToDelete").val();

            fetch('/api/Messages/' + messageId, {
                method: 'DELETE',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ id: messageId })
            });
        }

        self.messageHistory = function () {
            fetch('/api/chat/messagesbygroup/' + viewModel.joinedGroup().name())
                .then(response => response.json())
                .then(data => {
                    //console.log(data)
                    self.chatMessages.removeAll();
                    if (data.val) {

                        for (var i = 0; i < data.val.length; i++) {
                            var isMine = data.val[i].fromUserName == self.myProfile().userName();
                            self.chatMessages.push(new ChatMessage(data.val[i].id,
                                data.val[i].content,
                                data.val[i].timestamp,
                                data.val[i].fromUserName,
                                data.val[i].fromFullName,
                                isMine,
                                data.val.avatar))
                        }

                        $(".messages-container").animate({ scrollTop: $(".messages-container")[0].scrollHeight }, 1000);
                    }
                });
        }

        self.GroupAdded = function (Group) {
            self.chatGroups.push(Group);
        }

        self.GroupUpdated = function (updatedGroup) {
            var Group = ko.utils.arrayFirst(self.chatGroups(), function (item) {
                return updatedGroup.id() == item.id();
            });

            Group.name(updatedGroup.name());

            if (self.joinedGroup().id() == Group.id()) {
                self.joinGroup(Group);
            }
        }

        self.GroupDeleted = function (id) {
            var temp;
            ko.utils.arrayForEach(self.chatGroups(), function (Group) {
                if (Group.id() == id)
                    temp = Group;
            });
            self.chatGroups.remove(temp);
        }

        self.messageDeleted = function (id) {
            var temp;
            ko.utils.arrayForEach(self.chatMessages(), function (message) {
                if (message.id() == id)
                    temp = message;
            });
            self.chatMessages.remove(temp);
        }

        self.userAdded = function (user) {
            self.chatUsers.push(user);
        }

        self.userRemoved = function (id) {
            var temp;
            ko.utils.arrayForEach(self.chatUsers(), function (user) {
                if (user.userName() == id)
                    temp = user;
            });
            self.chatUsers.remove(temp);
        }

        self.uploadFiles = function () {
            var form = document.getElementById("uploadForm");

            $.ajax({
                type: "POST",
                url: '/api/Chat/SendMessageWithMediaUpload',
                data: new FormData(form),
                contentType: false,
                processData: false,
                success: function () {
                    $("#UploadedFile").val("");
                    $("#message-input").val("");
                },
                error: function (error) {
                    alert('Error: ' + error.responseText);
                }
            });
        }
    }

    function ChatGroup(id, name, admin) {
        var self = this;
        self.id = ko.observable(id);
        self.name = ko.observable(name);
        self.admin = ko.observable(admin);
    }

    function ChatUser(userName, fullName, currentGroup, device) {
        var self = this;
        self.userName = ko.observable(userName);
        self.fullName = ko.observable(fullName);
        self.currentGroup = ko.observable(currentGroup);
        self.device = ko.observable(device);
    }

    function ChatMessage(id, content, timestamp, fromUserName, fromFullName, isMine, avatar) {
        var self = this;
        self.id = ko.observable(id);
        self.content = ko.observable(content);
        self.timestamp = ko.observable(timestamp);
        self.timestampRelative = ko.pureComputed(function () {
            // Get diff
            var date = new Date(self.timestamp());
            var now = new Date();
            var diff = Math.round((date.getTime() - now.getTime()) / (1000 * 3600 * 24));

            // Format date
            var { dateOnly, timeOnly } = formatDate(date);

            // Generate relative datetime
            if (diff == 0)
                return `Today, ${timeOnly}`;
            if (diff == -1)
                return `Yestrday, ${timeOnly}`;

            return `${dateOnly}`;
        });
        self.timestampFull = ko.pureComputed(function () {
            var date = new Date(self.timestamp());
            var { fullDateTime } = formatDate(date);
            return fullDateTime;
        });
        self.fromUserName = ko.observable(fromUserName);
        self.fromFullName = ko.observable(fromFullName);
        self.isMine = ko.observable(isMine);
        self.avatar = ko.observable(avatar);
    }

    function ProfileInfo(userName, fullName, avatar) {
        var self = this;
        self.userName = ko.observable(userName);
        self.fullName = ko.observable(fullName);
        self.avatar = ko.observable(avatar);
    }

    function formatDate(date) {
        // Get fields
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var day = date.getDate();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var d = hours >= 12 ? "PM" : "AM";

        // Correction
        if (hours > 12)
            hours = hours % 12;

        if (minutes < 10)
            minutes = "0" + minutes;

        // Result
        var dateOnly = `${day}/${month}/${year}`;
        var timeOnly = `${hours}:${minutes} ${d}`;
        var fullDateTime = `${dateOnly} ${timeOnly}`;

        return { dateOnly, timeOnly, fullDateTime };
    }

    var viewModel = new AppViewModel();
    ko.applyBindings(viewModel);
});
